package br.ucsal.gcm.fatorial;

public class Fatorial {

	public static void main(String[] args) {
	
	}

	public static Long CalcularFatorial(Long num) {
		Long fat = 1l;

		for (int i = 1; i <= num; i++) {
			fat *= i;
		}
		return fat;
	}
}
