package br.ucsal.gcm.fatorial;

import org.junit.Assert;
import org.junit.Test;

public class FatorialTeste {

	@Test
	public void calcularFatorial1Test() {
		Long num = 1L;
		Long fatorialEsperado = 1L;

		Long fatorialAtual = Fatorial.CalcularFatorial(num);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
	
	@Test
	public void calcularFatorial0Test() {
		Long num = 0L;
		Long fatorialEsperado = 1L;

		Long fatorialAtual = Fatorial.CalcularFatorial(num);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
}
